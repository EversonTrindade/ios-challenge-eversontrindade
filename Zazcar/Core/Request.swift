//
//  Request.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/28/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct API {
    static let serverAPI    = "https://api.github.com/"
    static let repository   = "search/repositories"
    static let repos        = "repos/"
}

enum CallType {
    case pulls
    
    var url : String{
        switch self {
        case .pulls:
            return "pulls"
        }
    }
}

struct Request {
    static func requestRepositories(param: ([String : String]),
                                    success: @escaping (([Repository]) -> ()),
                                    failure: @escaping ((String) -> ())){
        if Reachability.isConnectedToNetwork() {
            Alamofire.request(API.serverAPI + API.repository,
                              method: .get,
                              parameters: param).validate().responseJSON{ response in
                                switch response.result{
                                case .success:
                                    guard let value = response.result.value else{
                                        failure("Falha no servidor")
                                        return
                                    }
                                    success(Repository.createRepositoriesFromHome(json: JSON(value)))
                                    break
                                case .failure:
                                    failure("Falha no servidor")
                                    break
                                }
            }
        }else{
            failure("Sem Conexão")
        }
    }
    
    static func requestRepositoryPulls(param: String,
                                       success: @escaping (([PullRequest]) -> ()),
                                       failure: @escaping ((String) -> ())){
        if Reachability.isConnectedToNetwork() {
            let link = API.serverAPI + API.repos + param + CallType.pulls.url
            Alamofire.request(link,
                              method: .get).validate().responseJSON{ response in
                                switch response.result{
                                case .success:
                                    guard let value = response.result.value else{
                                        failure("Falha no servidor")
                                        return
                                    }
                                    success(PullRequest.createPullRequest(json: JSON(value)))
                                    break
                                case .failure:
                                    failure("Falha no servidor")
                                    break
                                }
            }
        }else{
            failure("Sem Conexão")
        }
    }
    
}
