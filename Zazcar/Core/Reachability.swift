//
//
//  Created by Everson P. Trindade on 11/4/16.
//  Copyright © 2016 Everson P. Trindade. All rights reserved.
//

import Foundation
import SystemConfiguration

public class Reachability{
    
    var reachability = Reachability()
    
    class func isConnectedToNetwork() -> Bool{
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let ref: SCNetworkReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags = SCNetworkReachabilityFlags()
        let gotFlags = withUnsafeMutablePointer(to: &flags) {
            SCNetworkReachabilityGetFlags(ref, UnsafeMutablePointer($0))
        }
        
        if !gotFlags {
            return false
        }
        
        return flags.contains(.reachable) && !flags.contains(.connectionRequired)
    }

}
