//
//  WebBrowserProtocol.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

protocol WebBrowserPresentation{
    func setViewController(viewController: WebBrowserViewController)
    func loadView()
    func networkActivityIndicatorVisible()
    func networkActivityIndicatorNotVisible()
    func showMore()
}
