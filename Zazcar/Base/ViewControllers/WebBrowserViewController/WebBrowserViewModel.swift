//
//  WebBrowserViewModel.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

class WebBrowserViewModel: NSObject, WebBrowserPresentation {
    var viewController: WebBrowserViewController?
    
    func setViewController(viewController: WebBrowserViewController){
        self.viewController = viewController
    }
    
    func loadView(){
        self.viewController?.title = self.viewController?.selectedPullRequest[0].prTitle
        self.loadWebView()
    }
    
    private func loadWebView() {
        if let url = URL(string: (self.viewController?.selectedPullRequest[0].prLink)!) {
            let request = URLRequest(url: url)
            self.viewController?.webView.loadRequest(request)
        }
    }
    
    //MARK: UIWebViewDelegate
    func networkActivityIndicatorVisible(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func networkActivityIndicatorNotVisible(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK: IBAction
    func showMore() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { UIAlertAction in }
        
        let browserAction = UIAlertAction(title: "Open on browser", style: UIAlertActionStyle.default){ UIAlertAction in
            UIApplication.shared.open(URL(string: (self.viewController?.selectedPullRequest[0].prLink)!)!, options: [:], completionHandler: nil)
        }
        let reloadAction = UIAlertAction(title: "Reload page", style: UIAlertActionStyle.default){ UIAlertAction in
            self.loadWebView()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(reloadAction)
        alertController.addAction(browserAction)
        self.viewController?.present(alertController, animated: true, completion: nil)
    }
}
