//
//  WebBrowserViewController.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import WebKit

class WebBrowserViewController: UIViewController, UIWebViewDelegate {
    let viewModel = WebBrowserViewModel() as WebBrowserPresentation
    var parentVC: PullRequestViewController?
    var selectedPullRequest = [PullRequest]()
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.setViewController(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.loadView()
    }
    
    //MARK: UIWebViewDelegate
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.viewModel.networkActivityIndicatorVisible()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.viewModel.networkActivityIndicatorNotVisible()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.viewModel.networkActivityIndicatorNotVisible()
    }
    
    //MARK: IBAction
    @IBAction func showMore(_ sender: Any) {
        self.viewModel.showMore()
    }

}
