//
//  HomeCell.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/28/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SDWebImage

struct HomeCellDTO {
    var repositoryName          = ""
    var repositoryDescription   = ""
    var repositoryForks         = 0
    var repositoryStars         = 0
    var userImage               = ""
    var userName                = ""
}

class HomeCell: UITableViewCell {
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var repositoryFork: UILabel!
    @IBOutlet weak var repositoryStar: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    func fillCell(homeCellDTO: HomeCellDTO) {
        self.repositoryName.text = homeCellDTO.repositoryName
        self.repositoryDescription.text = homeCellDTO.repositoryDescription
        self.repositoryFork.text = homeCellDTO.repositoryForks.description
        self.repositoryStar.text = homeCellDTO.repositoryStars.description
        self.userImage.sd_setImage(with: URL(string: homeCellDTO.userImage))
        self.userName.text = homeCellDTO.userName
        
        
        self.userImage.layer.cornerRadius = 15
        self.userImage.clipsToBounds = true
    }
}
