//
//  HomeProtocol.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/23/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

protocol HomePresentation {
    func setViewController(viewController: HomeViewController)
    func loadView()
    func numberOfRowsInSection() -> Int
    func tableCellForIndexPath(tableDTO: HomeTableViewDTO) -> HomeTableViewDTO
    func didSelectRepositoryByIndexPath(indexPath: IndexPath)
    func prepareForSegue(segue: UIStoryboardSegue)
    func willDisplayCell(tableView: UITableView)
    func heightForRowAtIndexPath() -> CGFloat
    func updateSearchResults(searchController: UISearchController)
}
