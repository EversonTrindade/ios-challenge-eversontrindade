//
//  HomeViewModel.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/23/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SVProgressHUD

class HomeViewModel: NSObject, HomePresentation {
    private var viewController: HomeViewController?
    private var repositories = [Repository]()
    private var selectedRepository = [Repository]()
    private var filteredRepository = [Repository]()
    private var countOfPages = 1
    var searchController: UISearchController!
    
    func setViewController(viewController: HomeViewController) {
        self.viewController = viewController
    }
    
    func loadView() {
        self.searchBar()
        self.viewController?.title = "Github"
        self.viewController?.activityIndicator.hidesWhenStopped = true
        let param = ["q" : "language:Swift", "sort" : "stars", "page" : "1"]
        
        SVProgressHUD.show()
        self.listAllRepositories(param: param, successBlock: { success in
            SVProgressHUD.dismiss()
            self.reloadTableView()
        }) { failure in
            SVProgressHUD.dismiss()
            self.showAlert(message: failure)
        }
    }
    
    private func searchBar(){
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self.viewController
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.placeholder = "Search by name"
        self.viewController?.tableView.tableHeaderView = self.searchController.searchBar
    }
    
    //MARK: REQUEST
    private func listAllRepositories(param: ([String : String]),
                                     successBlock: @escaping ((Void) -> ()),
                                     failureBlock: @escaping ((String) -> ())){
        Request.requestRepositories(param: param,
                                    success: { respond in
                                        self.repositories = self.repositories + respond
                                        successBlock()
        }){ failure in
            failureBlock(failure)
        }
    }
    
    //MARK: TableView DTO
    func numberOfRowsInSection() -> Int {
        if self.searchController.isActive {
            return self.filteredRepository.count
        }else{
            return self.repositories.count
        }
        
    }
    
    func tableCellForIndexPath(tableDTO: HomeTableViewDTO) -> HomeTableViewDTO {
        let cell = tableDTO.tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: tableDTO.indexPath) as! HomeCell
        if self.searchController.isActive {
            cell.fillCell(homeCellDTO: createRepositoryDTO(repository: self.filteredRepository[tableDTO.indexPath.row]))
        }else{
            cell.fillCell(homeCellDTO: createRepositoryDTO(repository: self.repositories[tableDTO.indexPath.row]))
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return HomeTableViewDTO(tableView: tableDTO.tableView, cell: cell, indexPath: tableDTO.indexPath)
    }
    
    func didSelectRepositoryByIndexPath(indexPath: IndexPath) {
        if self.searchController.isActive {
            self.selectedRepository = [self.filteredRepository[indexPath.row]]
        }else{
            self.selectedRepository = [self.repositories[indexPath.row]]
        }
        
        self.searchController.dismiss(animated: true, completion: nil)
        self.searchController.searchBar.text = ""
        self.viewController?.performSegue(withIdentifier: SegueIdentifiers.detailRepository, sender: self.viewController)
    }
    
    func willDisplayCell(tableView: UITableView) {
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height) {
            self.callActivityIndicatorView()
        }
    }
    
    func heightForRowAtIndexPath() -> CGFloat {
        return 142.0
    }
    
    //MARK: DTO
    private func createRepositoryDTO(repository: Repository) -> HomeCellDTO {
        return HomeCellDTO(repositoryName: repository.repoName, repositoryDescription: repository.repoDesc, repositoryForks: repository.repoForks, repositoryStars: repository.repoStars, userImage: repository.repoOwnerAvatar, userName: repository.repoOwnerLogin)
    }
    
    //MARK: Alert Controller
    private func showAlert(message: String){
        let alertController = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in }
        alertController.addAction(okAction)
        self.viewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Segue
    func prepareForSegue(segue: UIStoryboardSegue) {
        if segue.identifier == SegueIdentifiers.detailRepository {
            if let viewController = segue.destination as? PullRequestViewController {
                viewController.parentVC = self.viewController
                viewController.selectedRepository = self.selectedRepository
            }
        }
    }
    
    //MARK: ActivityIndicatorView
    private func callActivityIndicatorView() {
        if !self.searchController.isActive {
            self.viewController?.activityIndicator.startAnimating()
            let param = ["q" : "language:Swift", "sort" : "stars", "page" : "\(self.countOfPages)"]
            self.listAllRepositories(param: param, successBlock: { success in
                self.viewController?.activityIndicator.stopAnimating()
                self.reloadTableView()
            }){ failure in
                self.viewController?.activityIndicator.stopAnimating()
                self.showAlert(message: failure)
            }
        }
    }
    
    //MARK: Reload Table
    private func reloadTableView(){
        self.countOfPages += 1
        DispatchQueue.main.async {
            self.viewController?.tableView.reloadData()
        }
    }
    
    //MARK: UISearchResultsUpdating
    func updateSearchResults(searchController: UISearchController){
        if let searchText = searchController.searchBar.text {
            self.filteredRepository = searchText.isEmpty ? repositories : repositories.filter { (item) -> Bool in
                return item.repoName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
            DispatchQueue.main.async {
                self.viewController?.tableView.reloadData()
            }
        }
    }
    
}
