//
//  HomeViewController.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/23/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

struct HomeTableViewDTO {
    var tableView = UITableView()
    var cell = UITableViewCell()
    var indexPath = IndexPath()
}

struct SegueIdentifiers {
    static let detailRepository = "goToRepositoryDetail"
    static let webBrowser       = "goToWebBrowser"
}

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating{
    let viewModel = HomeViewModel() as HomePresentation
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.setViewController(viewController: self)
        self.viewModel.loadView()
    }
    
    //MARK: UITableViewDelegate/UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.viewModel.tableCellForIndexPath(tableDTO: HomeTableViewDTO(tableView: tableView, cell: UITableViewCell(), indexPath: indexPath)).cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.didSelectRepositoryByIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewModel.willDisplayCell(tableView: tableView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.heightForRowAtIndexPath()
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.viewModel.prepareForSegue(segue: segue)
    }

    //MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        self.viewModel.updateSearchResults(searchController: searchController)
    }
}
