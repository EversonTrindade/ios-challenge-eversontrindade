//
//  PullRequestCell.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SDWebImage

struct PullRequestCellDTO {
    var prTitle         = ""
    var prBody          = ""
    var prUserName      = ""
    var prUserAvatar    = ""
    var prDate          = ""
}

class PullRequestCell: UITableViewCell {
    @IBOutlet weak var prTitle: UILabel!
    @IBOutlet weak var prBody: UILabel!
    @IBOutlet weak var prUserAvatar: UIImageView!
    @IBOutlet weak var prUserName: UILabel!
    @IBOutlet weak var prDate: UILabel!
    
    func fillCell(pullRequestCellDTO: PullRequestCellDTO) {
        self.prTitle.text = pullRequestCellDTO.prTitle
        self.prBody.text = pullRequestCellDTO.prBody
        self.prUserAvatar.sd_setImage(with: URL(string: pullRequestCellDTO.prUserAvatar))
        self.prUserName.text = pullRequestCellDTO.prUserName
        self.prDate.text = pullRequestCellDTO.prDate
        
        self.prUserAvatar.layer.cornerRadius = 15
        self.prUserAvatar.clipsToBounds = true
    }
}
