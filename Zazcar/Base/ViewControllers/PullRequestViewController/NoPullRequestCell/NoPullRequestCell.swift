//
//  NoPullRequestCell.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

struct NoPullRequestCellDTO {
    var title = ""
}

class NoPullRequestCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    func fillCell(noPullRequestCellDTO: NoPullRequestCellDTO){
        self.title.text = noPullRequestCellDTO.title
    }
}
