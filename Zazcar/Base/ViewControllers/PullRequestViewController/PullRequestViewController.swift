//
//  PullRequestViewController.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

struct PullRequestTableViewDTO {
    var tableView = UITableView()
    var cell = UITableViewCell()
    var indexPath = IndexPath()
}

class PullRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UISearchResultsUpdating{
    let viewModel = PullRequestViewModel() as PullRequestPresentation
    var parentVC: HomeViewController?
    var selectedRepository = [Repository]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.setViewController(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.loadView()
    }

    //MARK: UITableViewDelegate/UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.tableCellForIndexPath(tableDTO: PullRequestTableViewDTO(tableView: tableView, cell: UITableViewCell(), indexPath: indexPath)).cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.didSelectRepositoryByIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.heightForRowAtIndexPath()
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.viewModel.prepareForSegue(segue: segue)
    }
    
    //MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        self.viewModel.updateSearchResults(searchController: searchController)
    }
    
    //MARK: @IBAction
    @IBAction func moreButton(_ sender: Any) {
        self.viewModel.moreButton()
    }
}
