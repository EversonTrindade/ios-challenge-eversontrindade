//
//  PullRequestProtocol.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit

protocol PullRequestPresentation {
    func setViewController(viewController: PullRequestViewController)
    func loadView()
    func numberOfRowsInSection() -> Int
    func tableCellForIndexPath(tableDTO: PullRequestTableViewDTO) -> PullRequestTableViewDTO
    func didSelectRepositoryByIndexPath(indexPath: IndexPath)
    func prepareForSegue(segue: UIStoryboardSegue)
    func heightForRowAtIndexPath() -> CGFloat
    func updateSearchResults(searchController: UISearchController)
    func moreButton()
}
