//
//  PullRequestViewModel.swift
//  Zazcar
//
//  Created by Everson P. Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SVProgressHUD

class PullRequestViewModel: NSObject, PullRequestPresentation {
    private var viewController: PullRequestViewController?
    private var pullRequests = [PullRequest]()
    private var selectedPR = [PullRequest]()
    private var filteredPR = [PullRequest]()
    
    var searchController: UISearchController!

    func setViewController(viewController: PullRequestViewController){
        self.viewController = viewController
    }
    
    func loadView(){
        self.searchBar()
        self.viewController?.title = self.viewController?.selectedRepository[0].repoName
        self.viewController?.tableView.tableFooterView = UIView()
        SVProgressHUD.show()
        let param = (self.viewController?.selectedRepository[0].repoOwnerLogin)! + "/" + (self.viewController?.selectedRepository[0].repoName)! + "/"
        self.listAllPullRequests(param: param, successBlock: { success in
            SVProgressHUD.dismiss()
            DispatchQueue.main.async {
                self.viewController?.tableView.reloadData()
            }
        }){ failure in
            SVProgressHUD.dismiss()
            let alertController = UIAlertController(title: failure, message: nil, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { UIAlertAction in }
            alertController.addAction(okAction)
            self.viewController?.present(alertController, animated: true, completion: nil)
        }
    }

    private func searchBar(){
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self.viewController
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.placeholder = "Search by author name"
        self.viewController?.tableView.tableHeaderView = self.searchController.searchBar
    }
    
    //MARK: IBAction
    func moreButton() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { UIAlertAction in }
        
        let browserAction = UIAlertAction(title: "Open Repository on browser", style: UIAlertActionStyle.default){ UIAlertAction in
            UIApplication.shared.open(URL(string: (self.viewController?.selectedRepository[0].repoLink)!)!, options: [:], completionHandler: nil)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(browserAction)
        self.viewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: REQUEST
    private func listAllPullRequests(param: String,
                                     successBlock: @escaping ((Void) -> ()),
                                     failureBlock: @escaping ((String) -> ())){
        Request.requestRepositoryPulls(param: param, success: { respond in
            self.pullRequests = respond
            successBlock()
        }){ failure in
            failureBlock(failure)
        }
    }
    
    //MARK: TableView DTO
    func numberOfRowsInSection() -> Int{
        if self.pullRequests.count > 0 {
            if self.searchController.isActive{
                return filteredPR.count
            }else{
                return pullRequests.count
            }
        }else{
            return 1
        }
    }
    
    func tableCellForIndexPath(tableDTO: PullRequestTableViewDTO) -> PullRequestTableViewDTO{
        if self.pullRequests.count > 0 {
            let prCell = tableDTO.tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: tableDTO.indexPath) as! PullRequestCell
            if self.searchController.isActive{
                prCell.fillCell(pullRequestCellDTO: self.createPullRequestDTO(pullRequest: self.filteredPR[tableDTO.indexPath.row]))
            }else{
                prCell.fillCell(pullRequestCellDTO: self.createPullRequestDTO(pullRequest: self.pullRequests[tableDTO.indexPath.row]))
            }
            prCell.selectionStyle = UITableViewCellSelectionStyle.none
            return PullRequestTableViewDTO(tableView: tableDTO.tableView, cell: prCell, indexPath: tableDTO.indexPath)
        }else{
            let nprCell = tableDTO.tableView.dequeueReusableCell(withIdentifier: "NoPullRequestCell", for: tableDTO.indexPath) as! NoPullRequestCell
            nprCell.fillCell(noPullRequestCellDTO: self.creteNoPullRequestDTO())
            nprCell.selectionStyle = UITableViewCellSelectionStyle.none
            return PullRequestTableViewDTO(tableView: tableDTO.tableView, cell: nprCell, indexPath: tableDTO.indexPath)
        }
    }
    
    func didSelectRepositoryByIndexPath(indexPath: IndexPath) {
        if self.searchController.isActive {
            self.selectedPR = [self.filteredPR[indexPath.row]]
        }else{
            self.selectedPR = [self.pullRequests[indexPath.row]]
        }
        
       
//        self.viewController?.performSegue(withIdentifier: SegueIdentifiers.webBrowser, sender: self.viewController)
        
        //OR
        
        UIApplication.shared.open(URL(string: (self.selectedPR[0].prLink))!, options: [:], completionHandler: nil)
    }
    
    func heightForRowAtIndexPath() -> CGFloat {
        if self.pullRequests.count > 0 {
            return 180.0
        }else{
            return (self.viewController?.tableView.frame.size.height)!
        }
    }
    
    //MARK: DTO
    private func createPullRequestDTO(pullRequest: PullRequest) -> PullRequestCellDTO {
        return PullRequestCellDTO(prTitle: pullRequest.prTitle, prBody: pullRequest.prBody, prUserName: pullRequest.prOwnerName, prUserAvatar: pullRequest.prOwnerAvatar, prDate: pullRequest.prDate)
    }
    
    private func creteNoPullRequestDTO() -> NoPullRequestCellDTO {
        return NoPullRequestCellDTO(title: "No Pull Requests")
    }
    
    //MARK: Segue
    func prepareForSegue(segue: UIStoryboardSegue) {
        if segue.identifier == SegueIdentifiers.webBrowser{
            if let viewController = segue.destination as? WebBrowserViewController{
                viewController.parentVC = self.viewController
                viewController.selectedPullRequest = self.selectedPR
            }
        }
    }
    //MARK: UISearchResultsUpdating
    func updateSearchResults(searchController: UISearchController){
        if let searchText = searchController.searchBar.text {
            self.filteredPR = searchText.isEmpty ? pullRequests : pullRequests.filter { (item) -> Bool in
                return item.prOwnerName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            }
            DispatchQueue.main.async {
                self.viewController?.tableView.reloadData()
            }
        }
    }

}
