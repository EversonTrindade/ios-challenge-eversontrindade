//
//  PullRequest.swift
//  Zazcar
//
//  Created by Everson Trindade on 3/3/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequest: NSObject {
    dynamic var prId            = ""
    dynamic var prOwnerName     = ""
    dynamic var prOwnerAvatar   = ""
    dynamic var prTitle         = ""
    dynamic var prDate          = ""
    dynamic var prBody          = ""
    dynamic var prLink          = ""

    class func createPullRequest(json: JSON) -> [PullRequest]{
        var pullRequests = [PullRequest]()
        for dict in json {
            let pullRequest = PullRequest()
            pullRequest.prId            = dict.1["id"].string ?? ""
            pullRequest.prOwnerName     = dict.1["user"]["login"].string ?? ""
            pullRequest.prOwnerAvatar   = dict.1["user"]["avatar_url"].string ?? ""
            pullRequest.prTitle         = dict.1["title"].string ?? ""
            pullRequest.prDate          = dict.1["created_at"].string ?? ""
            pullRequest.prBody          = dict.1["body"].string ?? ""
            pullRequest.prLink          = dict.1["_links"]["html"]["href"].string ?? ""
            
            pullRequests.append(pullRequest)
        }
        return pullRequests
    }
}
