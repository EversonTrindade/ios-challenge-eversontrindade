//
//  Repository.swift
//  Zazcar
//
//  Created by Everson Trindade on 2/28/17.
//  Copyright © 2017 sp.ios.code.zazcar.test. All rights reserved.
//

import UIKit
import SwiftyJSON

class Repository: NSObject {
    dynamic var repoId          = ""
    dynamic var repoName        = ""
    dynamic var repoDesc        = ""
    dynamic var repoForks       = 0
    dynamic var repoStars       = 0
    dynamic var repoLink        = ""
    dynamic var repoOwnerAvatar = ""
    dynamic var repoOwnerLogin  = ""

    class func createRepositoriesFromHome(json: JSON) -> [Repository]{
        var repositories = [Repository]()
        for dict in json["items"] {
            let repository = Repository()
            repository.repoId           = dict.1["id"].string ?? ""
            repository.repoName         = dict.1["name"].string ?? ""
            repository.repoDesc         = dict.1["description"].string ?? ""
            repository.repoForks        = dict.1["forks_count"].int ?? 0
            repository.repoStars        = dict.1["stargazers_count"].int ?? 0
            repository.repoLink         = dict.1["svn_url"].string ?? ""
            repository.repoOwnerLogin   = dict.1["owner"]["login"].string ?? ""
            repository.repoOwnerAvatar  = dict.1["owner"]["avatar_url"].string ?? ""
            
            repositories.append(repository)
        }
        return repositories
    }
}
